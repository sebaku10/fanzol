CREATE DATABASE IF NOT EXISTS `fanzol`;
USE fanzol;

grant all on `fanzol`.* to 'fanzoladmin' identified by 'Fanzol12#';

-- CREATE USER 'fanzoladmin' IDENTIFIED BY 'Fanzol12#';

-- grant usage on *.* to fanzoladmin@localhost identified by 'Fanzol12#';
-- grant all privileged on fanzol.* to fanzoladmin@localhost;
-- DOmena z adresem ip do serwera z ktorym nalezy sie polaczyzc na stronie internetowej.



CREATE TABLE IF NOT EXISTS `sessions` (
  `user_id` int(11) NOT NULL,
  `skey` varchar(45) NOT NULL,
  `lasts` date NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `key_UNIQUE` (`skey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  `activation` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(60) DEFAULT 'none',
  `avatar` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `friends` (
  `user1` int(11) NOT NULL,
  `user2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;