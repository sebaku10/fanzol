package klient;

import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.UnknownHostException;

public interface IClient {
	public void connectToServer(String serverHost, int serverPort, int userID)
			throws UnknownHostException, IOException;
		public void sendMessesage(int userID, String messesage)
				throws IOException;
	public void run();
	public void addPropertyChangeListener(PropertyChangeListener listener);
	public void removePropertyChangeListener(PropertyChangeListener listener) ;
}
