/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package klient;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;

/**
 *
 * @author Tomek
 */
public class Klient {
    public static void main(String[] args) throws IOException {

        String serverHost = "127.0.0.1"; // adres IP serwera ("cyfrowo" lub z użyciem DNS)
        int serverPort = 5123;
        Client client = new Client();

        client.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent arg0) {
                System.out.println(arg0.getOldValue());
                System.out.println(arg0.getNewValue());
            }
        });
        client.connectToServer(serverHost, serverPort, 3);
    }
}
