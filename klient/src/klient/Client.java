package klient;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client implements Runnable, IClient {

    int userID;

    public final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    private Socket socket = null;

    public void connectToServer(String serverHost, int serverPort, int userID)
            throws UnknownHostException, IOException {

        this.userID = userID;
        //this.userName = userName;
        this.socket = new Socket(serverHost, serverPort);

        // przesłanie do servera swojego id
        PrintWriter outp;
        outp = new PrintWriter(this.socket.getOutputStream());
        outp.println(userID);
        outp.flush();

        new Thread(this).start();
    }

    public int getUserID() {
        return this.userID;
    }

    public void sendMessesage(int reciverID, String messesage)
            throws IOException {
        PrintWriter outp;
        outp = new PrintWriter(this.socket.getOutputStream());
        outp.println(reciverID + "|" + this.userID + "|" + messesage);
        outp.flush();

        // outp.close();
    }

    public void run() {
        while (true) {
            String messesage = null;
            BufferedReader intput = null;
            try {
                intput = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
                messesage = intput.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String[] splitMessage = splitMessage(messesage);

            pcs.firePropertyChange("messesage", splitMessage[0], splitMessage[1]);
        }
    }

    private String[] splitMessage(String messesage) {
        String senderID = "";
        String newMessesage = "";
        int endIndex = 0;

        for (int i = 0; i < messesage.length(); i++) {
            if (messesage.charAt(i) == '|') {
                endIndex = i + 1;
                break;
            }
            senderID += messesage.charAt(i);
        }

        for (int i = endIndex; i < messesage.length(); i++) {
            newMessesage += messesage.charAt(i);
        }

        return new String[]{senderID, newMessesage};
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }
}
