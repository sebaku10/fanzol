/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serwer;

import java.io.*;
import java.net.Socket;

/**
 *
 * @author Tomek
 */
public class KlientStruktura {

    int userID;
    Socket sock;
    BufferedReader inp;
    PrintWriter outp;

    public KlientStruktura(int userID, Socket sock, BufferedReader inp, PrintWriter outp) {
        this.userID = userID;
        this.sock = sock;
        this.inp = inp;
        this.outp = outp;
    }
}
