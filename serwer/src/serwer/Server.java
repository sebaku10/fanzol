package serwer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import serwer.KlientStruktura;

public class Server implements IServer {

    private List<KlientStruktura> listaKlientow;

    int tempInt = 0;
    ServerSocket serv;
    Socket sock = null;

    private final Runnable typeA;
    private final Runnable typeB;

    public Server(int serverPort) throws IOException {

        this.serv = new ServerSocket(serverPort);
        listaKlientow = new ArrayList<KlientStruktura>();

        typeA = new Runnable() {
            public void run() {
                try {
                    Server.this.listenClients();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        typeB = new Runnable() {
            public void run() {
                try {
                    Server.this.sendMessesageToClient();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        new Thread(typeA).start();
        new Thread(typeB).start();
    }

    private void listeningClients() throws IOException {
        System.out.println("Nasluchuje: " + serv);
        sock = serv.accept();

        BufferedReader intput = new BufferedReader(new InputStreamReader(sock.getInputStream()));
        PrintWriter output = new PrintWriter(sock.getOutputStream());

        String str = null;
        str = intput.readLine();

        if (containClient(Integer.parseInt(str), listaKlientow, sock, intput, output)); else {
            listaKlientow.add(new KlientStruktura(Integer.parseInt(str), sock, intput, output));
        }
    }

    public void listenClients() throws IOException {
        while (true) {
            listeningClients();
        }
    }

    public void sendMessesageToClient() throws Exception {
        while (true) {
            if (listaKlientow != null) {
                for (int i = 0; i < listaKlientow.size(); i++) {
                    try {
                        if (listaKlientow.get(i).inp.ready()) {
                            String str = null;
                            str = listaKlientow.get(i).inp.readLine();

                            String[] splitStr = splitMessesage(str);
                            int klientIndex = findClient(Integer.parseInt(splitStr[0]), listaKlientow);

                            if (klientIndex < 0) {
                                listaKlientow.get(i).outp.println("Brak użytkownika do którego ma zostać wysłana wiadomość");
                                listaKlientow.get(i).outp.flush();
                            } else {
                                listaKlientow.get(klientIndex).outp.println(splitStr[1] + "|" + splitStr[2]);
                                listaKlientow.get(klientIndex).outp.flush();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private Boolean containClient(int userID, List<KlientStruktura> clientsList, Socket sock, BufferedReader intput, PrintWriter output) {
        int counter = 0;
        for (KlientStruktura klient : clientsList) {
            if (klient.userID == userID) {
                listaKlientow.get(counter).sock = sock;
                listaKlientow.get(counter).inp = intput;
                listaKlientow.get(counter).outp = output;
                return true;
            }
            counter++;
        }
        return false;
    }

    private String[] splitMessesage(String messesage) {
        String reciverID = "";
        String senderID = "";
        String newMessesage = "";
        int endIndex = 0;

        for (int i = 0; i < messesage.length(); i++) {
            if (messesage.charAt(i) == '|') {
                endIndex = i + 1;
                break;
            }
            reciverID += messesage.charAt(i);
        }

        for (int i = endIndex; i < messesage.length(); i++) {
            if (messesage.charAt(i) == '|') {
                endIndex = i + 1;
                break;
            }
            senderID += messesage.charAt(i);
        }

        for (int i = endIndex; i < messesage.length(); i++) {
            newMessesage += messesage.charAt(i);
        }

        return new String[]{reciverID, senderID, newMessesage};
    }

    private int findClient(int userID, List<KlientStruktura> listaKlientow) {
        int counter = 0;
        for (KlientStruktura klient : listaKlientow) {
            if (klient.userID == userID) {
                break;
            }
            counter++;
        }

        if (counter == listaKlientow.size()) {
            counter = -1;
        }
        return counter;
    }
}
