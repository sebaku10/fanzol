## **1. Opis programu** ##

Głównym celem projektu będzie stworzenie aplikacji do komunikacji internetowej. Program będzie wymagał zalogowania do systemu, a więc także stworzenie konta. Każdy zalogowany użytkownik będzie mógł wyszukiwać innych użytkowników, dodawać do znajomych. Dodanie do znajomych będzie umożliwiało nawiązanie komunikacji tekstowej. Podstawowe funkcjonalności:

- Tworzenie kont

- Konieczność autoryzacji/logowania

- Komunikacja tekstowa między użytkownikami

- Możliwość dodawania użytkowników do listy znajomych

- Archiwum przechowujące rozmowy

- Dodatkowe funkcjonalności społecznościowe m.in dodanie zdjęcia/avatara

Dokumentację można znaleźć tutaj: https://docs.google.com/document/d/1yDpNbVtq3GHhhu33iSXC2mFiuOh5qECclBZbUKicRrs/edit

## **2. Instalacja i uruchamianie** ##

Program do poprawnego działania wymaga:

a) Uruchomiony server do wysyłania wiadomości.  
b) Uruchomiony server mysql.  

* ### Instalacja MySQL ###
Na komputerze należy uruchomić plik *install_db.bat* który pobierze mysql oraz utworzy potrzebne tablele.    

* ### Uruchamianie serwera do wymiany wiadomości ###
Należy włączyć plik run_fanzol_server.bat (lub run_fanzol_server.sh na linux).  

* ### Uruchamianie programu "FANZOL" ###
Po uruchomieniu pliku run_fanzol_main.bat (lub run_fanzol_main.sh na linux) ukaże nam się okno:

![fanzol.png](https://bitbucket.org/repo/jX4jMM/images/2532125791-fanzol.png)

Po wpisaniu localhost i kliknięciu połącz program nawiąże połączenie z serwerem. Teraz można korzystać z funkcjonalności wymienionych w dokumentacji.