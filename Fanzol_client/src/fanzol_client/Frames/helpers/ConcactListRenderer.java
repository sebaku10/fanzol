/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fanzol_client.Frames.helpers;

import components.database.Database;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author Mateusz
 */
public class ConcactListRenderer extends JLabel implements ListCellRenderer<Object> {
    private Database db = null;
    
    public ConcactListRenderer(Database base) {
        this.db = base;
        setOpaque(true);
    }

    public Component getListCellRendererComponent(JList<?> list,
                                                  Object value,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus) {

        setText(value.toString());

        Color background = Color.WHITE;
        Color foreground;
        Font font;
        
     
        

        if (isSelected) {
            background = new Color(100, 200, 100, 50);
        } 
        
         // Jesli uzytkownik jest online to zmieniamy kolor na zielony i dajemy Font.BOLD.
        if(db.isUserAlreadyLoggedIn(value.toString())) {
           font = new Font("Courier New", Font.BOLD, 12);
           foreground = new Color(10, 200, 90);
        } else {
           foreground = new Color(70, 80, 80); 
        }

        setBackground(background);
        setForeground(foreground);
        setFont(null);

        return this;
    }
}
