package fanzol_client.Frames;

import archive.IArchive;
import components.authentication.interfaces.IAuthentication;
import components.database.Database;
import fanzol_client.Frames.helpers.ConcactListRenderer;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;
import javax.swing.JList;
import javax.xml.parsers.ParserConfigurationException;
import klient.*;


public class MenuFrame extends javax.swing.JFrame {

    private final IClient client; 
    private final SearchFrame search;
    private final IArchive archive;
    private final Database base;
    private final IAuthentication authentication;
    
    public MenuFrame(IClient client, SearchFrame search, IArchive archive, Database base, IAuthentication authentication) {
       
        initComponents();
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.authentication = authentication;
        this.client = client;
        this.search = search;
        this.archive = archive;
        this.base = base;
        
        List<MessageFrame> windows = new ArrayList<>();
        
        jList1.addMouseListener(new MouseAdapter( )
        {        
            @Override
            public void mouseClicked(MouseEvent evt) 
            {
                // Klikamy 2 razy na nazwe uzytkownika - wyskakuje okno komunikacji.
                if(evt.getClickCount() == 2) {
                JList list = (JList)evt.getSource();
                String l = list.getSelectedValue().toString();
                
                if(windows.isEmpty())
                {
                    MessageFrame message = new MessageFrame(client, archive, authentication.getUserName(), l, base.getUserId(l));
                    message.setVisible(true);
                    windows.add(message);       
                }
                else
                {
                    boolean help = true;
                    
                    for (MessageFrame key : windows) {
                        if(key.getId()==base.getUserId(l)) 
                        {
                            help=false;
                            key.setVisible(true);
                        }
                    }
                
                if(help)
                {
                    MessageFrame message = new MessageFrame(client, archive, authentication.getUserName(), l, base.getUserId(l));
                    message.setVisible(true);
                    windows.add(message);
                }                
                }
                }
            }
        });
        
        ConcactListRenderer concactListRenderer = new ConcactListRenderer(base);
        jList1.setCellRenderer(concactListRenderer);

        client.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent arg0) {
                
                if(windows.isEmpty())
                {
                    MessageFrame message = new MessageFrame(client, archive, authentication.getUserName(), base.getNameById(Integer.parseInt(arg0.getOldValue().toString())), Integer.parseInt(arg0.getOldValue().toString()));
                    message.setVisible(true);
                    message.recive(arg0.getNewValue().toString());
                    windows.add(message);       
                }
                else
                {
                    boolean help = true;
                    
                    for (MessageFrame key : windows) {
                        if(key.getId()==Integer.parseInt(arg0.getOldValue().toString())) 
                        {
                            help=false;
                            key.setVisible(true);
                            key.recive(arg0.getNewValue().toString());
                        }
                    }
                
                if(help)
                {
                    MessageFrame message = new MessageFrame(client, archive, authentication.getUserName(), base.getNameById(Integer.parseInt(arg0.getOldValue().toString())), Integer.parseInt(arg0.getOldValue().toString()));
                    message.setVisible(true);
                    message.recive(arg0.getNewValue().toString());
                    windows.add(message);
                }                
                }                

            }
            });    

}

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        LogoutButton = new javax.swing.JButton();
        SearchButton = new javax.swing.JButton();
        ArchiveButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        UserLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                formMouseEntered(evt);
            }
        });

        jScrollPane1.setViewportView(jList1);

        LogoutButton.setText("Logout");
        LogoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LogoutButtonActionPerformed(evt);
            }
        });

        SearchButton.setText("Wyszukaj");
        SearchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SearchButtonActionPerformed(evt);
            }
        });

        ArchiveButton.setText("Archiwum");
        ArchiveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ArchiveButtonActionPerformed(evt);
            }
        });

        jLabel3.setText("Kliknij na kontakcie aby zacząć konwersacje.");

        UserLabel.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        UserLabel.setForeground(new java.awt.Color(0, 153, 0));

        jLabel1.setForeground(new java.awt.Color(0, 153, 0));
        jLabel1.setText("Witaj!");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(LogoutButton))
                    .addComponent(UserLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ArchiveButton)
                            .addComponent(SearchButton)
                            .addComponent(jLabel1))
                        .addGap(0, 25, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(UserLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LogoutButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(SearchButton)
                        .addGap(18, 18, 18)
                        .addComponent(ArchiveButton)
                        .addGap(20, 20, 20))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void LogoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LogoutButtonActionPerformed
        authentication.logout();
        System.exit(0);
    }//GEN-LAST:event_LogoutButtonActionPerformed

    private void SearchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SearchButtonActionPerformed
                search.setVisible(true);
                search.setMe(authentication.getUserName());
                }//GEN-LAST:event_SearchButtonActionPerformed

    private void ArchiveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ArchiveButtonActionPerformed
                ArchiveFrame arch = new ArchiveFrame(archive);
                arch.setVisible(true);
                arch.setContacts(base.getUserContacts(authentication.getUserName()));
                }//GEN-LAST:event_ArchiveButtonActionPerformed

    private void formMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseEntered
        jList1.setListData(base.getUserContacts(authentication.getUserName()));
        UserLabel.setText(authentication.getUserName());       
    }//GEN-LAST:event_formMouseEntered

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ArchiveButton;
    private javax.swing.JButton LogoutButton;
    private javax.swing.JButton SearchButton;
    private javax.swing.JLabel UserLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JList jList1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
