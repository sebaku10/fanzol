package fanzol_client.Frames;

import components.activation.interfaces.IActivateUser;
import components.authentication.interfaces.IAuthentication;


public class RegisterFrame extends javax.swing.JFrame {

    private final IActivateUser activator;
    private final IAuthentication reg;
    
    public RegisterFrame(IActivateUser activator, IAuthentication reg) {
        
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        this.activator = activator;
        this.reg = reg;
    }
  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Logowanie = new javax.swing.JPanel();
        LogField = new javax.swing.JTextField();
        PassField = new javax.swing.JPasswordField();
        RegButton = new javax.swing.JButton();
        MailField = new javax.swing.JTextField();
        RePassField = new javax.swing.JPasswordField();
        jLabel1 = new javax.swing.JLabel();
        WarningField = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        LogField.setToolTipText("");

        RegButton.setText("Rejestruj");
        RegButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RegButtonActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Rejestracja:");

        WarningField.setForeground(new java.awt.Color(255, 0, 0));
        WarningField.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabel2.setText("nazwa użytkownika");

        jLabel3.setText("hasło");

        jLabel4.setText("powtórz hasło");

        jLabel5.setText("e-mail");

        javax.swing.GroupLayout LogowanieLayout = new javax.swing.GroupLayout(Logowanie);
        Logowanie.setLayout(LogowanieLayout);
        LogowanieLayout.setHorizontalGroup(
            LogowanieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(WarningField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(LogowanieLayout.createSequentialGroup()
                .addGroup(LogowanieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(LogowanieLayout.createSequentialGroup()
                        .addGap(93, 93, 93)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(LogowanieLayout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addGroup(LogowanieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, LogowanieLayout.createSequentialGroup()
                                .addGap(114, 114, 114)
                                .addComponent(RegButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(LogowanieLayout.createSequentialGroup()
                                .addGroup(LogowanieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(LogowanieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(PassField)
                                    .addComponent(LogField)
                                    .addComponent(MailField)
                                    .addComponent(RePassField, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(107, Short.MAX_VALUE))
        );
        LogowanieLayout.setVerticalGroup(
            LogowanieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LogowanieLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(WarningField, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19)
                .addGroup(LogowanieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LogField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(LogowanieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PassField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(LogowanieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RePassField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                .addGroup(LogowanieLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(MailField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addComponent(RegButton)
                .addGap(32, 32, 32))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Logowanie, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Logowanie, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void RegButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RegButtonActionPerformed

        String warning = "";
        
        if (!(LogField.getText().contains(" ")||LogField.getText().isEmpty()||(!PassField.getText().equals(RePassField.getText()))))
        {
            if(reg.register(LogField.getText(), PassField.getText(), MailField.getText(), null))
            {
                activator.sendActivationMessage(LogField.getText());
                this.dispose();
            }
            else warning = warning.concat("Nazwa użytkownika lub adres e-mail jest już używanie!");
        }
      
        if(LogField.getText().contains(" "))warning = warning.concat(" Nazwa użytkownika nie może zawierać spacji.");
        if(LogField.getText().isEmpty())warning = warning.concat("Nazwa użytkownika nie może być pusta! ");
        if(!PassField.getText().equals(RePassField.getText()))warning = warning.concat("Hasła nie są takie same!");

        WarningField.setText(warning);
        
        LogField.setText("");
        PassField.setText("");
        RePassField.setText("");
        MailField.setText("");
    }//GEN-LAST:event_RegButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField LogField;
    private javax.swing.JPanel Logowanie;
    private javax.swing.JTextField MailField;
    private javax.swing.JPasswordField PassField;
    private javax.swing.JPasswordField RePassField;
    private javax.swing.JButton RegButton;
    private javax.swing.JLabel WarningField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    // End of variables declaration//GEN-END:variables
}
