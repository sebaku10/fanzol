package fanzol_client.Frames;

import archive.IArchive;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import klient.IClient;
import org.xml.sax.SAXException;


public class MessageFrame extends javax.swing.JFrame {

    private final String me;
    private final String shared;
    private final int id;
    private final klient.IClient client;
    private final archive.IArchive archive;
    
    public int getId()
    {
        return id;
    }
    
    public void recive(String message)
    {
        jTextArea1.setText(jTextArea1.getText()+"\n\n"+shared+":\n"+message);
        try {
                archive.addNewConversation(me, shared, message);
        } catch (ParserConfigurationException | TransformerException | SAXException | IOException ex) {
        }
    }
    
    public MessageFrame(IClient client, IArchive archive, String me, String shared, int id) {
        
        initComponents();
        jLabel1.setText("Do "+shared);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.client = client;
        this.archive = archive;
        this.me = me;
        this.id = id;
        this.shared = shared;
//        client.addPropertyChangeListener(new PropertyChangeListener() {
//            @Override
//            public void propertyChange(PropertyChangeEvent arg0) {
//                jTextArea1.setText(jTextArea1.getText()+"\n\n+"+shared+":\n"+arg0.getNewValue());
//                try {
//                    archive.addNewConversation(me, shared, arg0.getNewValue().toString());
//                } catch (ParserConfigurationException | TransformerException | SAXException | IOException ex) {
//                    Logger.getLogger(MessageFrame.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//            });    
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        TextArea = new javax.swing.JTextArea();
        SendButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        TextArea.setColumns(20);
        TextArea.setRows(5);
        jScrollPane1.setViewportView(TextArea);

        SendButton.setText("Wyślij");
        SendButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SendButtonActionPerformed(evt);
            }
        });

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        jLabel1.setForeground(new java.awt.Color(0, 153, 0));
        jLabel1.setText("jLabel1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(SendButton))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(SendButton)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SendButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SendButtonActionPerformed
              
        try {
            client.sendMessesage(id, TextArea.getText());
        } catch (IOException ex) {
            jTextArea1.setText(jTextArea1.getText()+"\n\nPołączenie nieudane!");
        }

        try {
 
            jTextArea1.setText(jTextArea1.getText()+"\n\nDo "+shared+":\n"+TextArea.getText());
            archive.addNewConversation(shared, me, TextArea.getText());

        } catch (ParserConfigurationException | TransformerException | SAXException | IOException ex) {
            Logger.getLogger(MessageFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        TextArea.setText("");
    }//GEN-LAST:event_SendButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton SendButton;
    private javax.swing.JTextArea TextArea;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    // End of variables declaration//GEN-END:variables
}
