package fanzol_client;

import javax.swing.JFrame;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Fanzol_client {


    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("fanzol_client/Settings/beans.xml");
             
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                context.getBean("loginFrame", JFrame.class).setVisible(true);
            }
            });
    }
    
}
